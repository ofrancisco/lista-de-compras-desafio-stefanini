import {Produto} from '../Objetos/produto';
import {Component, OnInit} from '@angular/core';
import {ProdutoService} from '../service/produto.service';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-lista-produto',
    templateUrl: './lista-produto.component.html',
    styleUrls: ['./lista-produto.component.css']
})

export class ListaProdutoComponent implements OnInit {

    produtos: Array<Produto> = [];
    carregarLoading = false;
    public produtoEdicaoAtivada = false;

    public formProduto = this.formBuilder.group({
        nome: ['', Validators.required],
        comprado: [false]
    });
    public formEdicao = new FormGroup({});

    constructor(
        private produtoService: ProdutoService,
        private formBuilder: FormBuilder,
        private router: Router
    ) {

    }

    ngOnInit(): void {
        this.produtoService.listar().subscribe((produto: Produto[]) => {
            setTimeout(() => {
                this.carregarLoading = true;
                this.produtos = produto;
            }, 2000);
        });


    }

    public submitForm() {
        if (this.formProduto.valid) {
            const novoProduto: Produto = new Produto();
            novoProduto.nome = this.formProduto.getRawValue().nome;
            this.produtoService.criar(novoProduto).subscribe(produtoCadastrado => {
                this.produtos.push(produtoCadastrado);
            });
        }
    }

    public ativarEdicao(produto: Produto) {
        this.produtoEdicaoAtivada = !this.produtoEdicaoAtivada;
    }

    public excluirProduto(produto: Produto) {
        this.produtoService.deletar(produto.id).subscribe(produtosListados => {
            this.produtos = produtosListados;
        });
    }

    concluirCompra(produto: Produto) {
        produto.comprado = true;
        this.produtoService.atualizar(produto).subscribe();
    }
}
