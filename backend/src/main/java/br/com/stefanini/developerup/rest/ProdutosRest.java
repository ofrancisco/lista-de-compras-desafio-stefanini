package br.com.stefanini.developerup.rest;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.stefanini.developerup.model.Produto;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;

import br.com.stefanini.developerup.dto.ProdutoDto;
import br.com.stefanini.developerup.service.ProdutoService;

import java.util.List;


@RequestScoped
@Path("/produto")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProdutosRest {

    @Inject
    ProdutoService service;

    @GET
    @Operation(summary = "Listar", description = "Retorna uma lista dos produtos da lista de compras")
    @APIResponse(
        responseCode = "200",
        description = "ProdutoDto",
        content = {@Content(
            mediaType = "application/json", 
            schema = @Schema(implementation = ProdutoDto.class)
        )})
    public Response listar()  {
        return Response.status(Response.Status.OK).entity(service.listar()).build();
    }

    @POST
    @Operation(summary = "cadastrar",
            description = "Cadastra produto e retorna o produto cadastrado")
    @Transactional
    public Response cadastrar(Produto produto) {
        Produto produtoCadastrado = service.cadastrarProduto(produto);

        return Response.status(Response.Status.OK).entity(produtoCadastrado).build();
    }

    @PUT
    @Operation(summary = "atualizar",
            description = "Atualiza produto e retorna o produto atualizado")
    @Path("{id}")
    @Transactional
    public Response atualizar(@PathParam("id") Long id, ProdutoDto produtoDto) {
        Produto produtoAtualizado = service.atualizarProduto(id, produtoDto);
        return Response.status(Response.Status.OK).entity(produtoAtualizado).build();
    }


    @DELETE
    @Operation(summary = "deletar",
            description = "deleta produto através do id recebido e retorna a lista de produto restantes")
    @Path("{id}")
    @Transactional
    public Response deletar(@PathParam("id") Long id) {
        List<ProdutoDto> listaProduto = service.deletarProduto(id);
        return Response.status(Response.Status.OK).entity(listaProduto).build();
    }
}