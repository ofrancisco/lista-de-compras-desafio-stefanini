package br.com.stefanini.developerup.dao;

import br.com.stefanini.developerup.model.Produto;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import io.quarkus.panache.common.Sort;
import javax.enterprise.context.RequestScoped;
import java.util.List;
import java.util.Optional;


@RequestScoped
public class ProdutoDao {

    public List<Produto> listar(){
        return Produto.listAll(Sort.by("Id,comprado,nome").ascending());
    }

    public Optional<Produto> buscarPorId(Long id) {
        return Produto.findByIdOptional(id);
    }

    public boolean deletar(Long id) {
        return Produto.deleteById(id);
    }
}