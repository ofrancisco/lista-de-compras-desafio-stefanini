package br.com.stefanini.developerup.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;

import br.com.stefanini.developerup.dao.ProdutoDao;
import br.com.stefanini.developerup.dto.ProdutoDto;
import br.com.stefanini.developerup.model.Produto;


@RequestScoped
public class ProdutoService {

    @Inject
    ProdutoDao dao;
    
    public List<ProdutoDto> listar(){
        List<Produto> lista = dao.listar();
        List<ProdutoDto> listaDto = new ArrayList<>();
        for (Produto produto : lista) {
            ProdutoDto produtoDto = new ProdutoDto();
            produtoDto.setCodigo(produto.getId());
            produtoDto.setNome(produto.getNome());
            produtoDto.setComprado(produto.isComprado());
            listaDto.add(produtoDto);
        }
        return listaDto;
        // return dao.listar().stream().map(ProdutoParser.get()::dto).collect(Collectors.toList());
    }

    public Produto cadastrarProduto(Produto produto) {
        Produto produtoCadastrado = new Produto();
        produtoCadastrado.setNome(produto.getNome());
        produtoCadastrado.setComprado(produto.isComprado());
        produtoCadastrado.persist();

        return produtoCadastrado;
    }


    public Produto atualizarProduto(Long id, ProdutoDto produtoDto) {
        Optional<Produto> produtoOptional = dao.buscarPorId(id);

        if (produtoOptional.isPresent()) {
            Produto produtoAtualizado = produtoOptional.get();
            produtoAtualizado.setNome(produtoDto.getNome());
            produtoAtualizado.setComprado(produtoDto.isComprado());
            produtoAtualizado.persist();
            return produtoAtualizado;
        }

        throw new NotFoundException();
    }


    public List<ProdutoDto> deletarProduto(Long id) {
        Optional<Produto> produtoOptional = dao.buscarPorId(id);

        if(produtoOptional.isPresent()){
            produtoOptional.get().delete();
            return this.listar();
        }

        throw new NotFoundException();

    }
}