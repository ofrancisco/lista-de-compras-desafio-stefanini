import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { EnunciadoComponent } from './enunciado/enunciado.component';
import { ListaProdutoComponent } from './lista-produto/lista-produto.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'home', component: HomeComponent},
  {path: 'enunciado', component: EnunciadoComponent},
  {path: 'listar', component: ListaProdutoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})

export class AppRoutingModule { }
